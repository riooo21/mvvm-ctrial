//
//  CreateStoryVC.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 20/01/22.
//
// untuk storyboard agar lebih mudah di instatiate (masih belum xib)

import Foundation
import UIKit

protocol CreateStoryVC {
    static func instantiate() -> Self
}

extension CreateStoryVC where Self: UIViewController {
    static func instantiate() -> Self {
        let id = String(describing: self)
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateViewController(identifier: id) as! Self
    }
}
