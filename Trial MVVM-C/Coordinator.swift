//
//  Coordinator.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 20/01/22.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] {
        get set
    }
    var navigationController: UINavigationController {
        get set
    }
    
    func switchScreen()
}
