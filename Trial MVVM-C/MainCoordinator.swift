//
//  MainCoordinator.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 20/01/22.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func switchScreen() {
        let vc = ViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func changeEmail() {
        let vc = ChangeEmailViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func changeUsername() {
        let vc = ChangeUsernameViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func confirmEmail() {
        /*let vc = ViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)*/
        navigationController.popToRootViewController(animated: true)
    }
    
    func confirmUsername() {
        /*let vc = ViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)*/
        navigationController.popToRootViewController(animated: true)
    }
    
    func moveToXIBHomeScreen() {
        let vc = XIBScreenViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
}
