//
//  UserViewModel.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 25/01/22.
//

import Foundation
import UIKit

struct UserVM {
    var username: String
    var email: String
}
