//
//  UserModel.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 25/01/22.
//

import Foundation
import UIKit

public class User {
    public let email: String
    public let username: String
    
    public init(email: String, username: String) {
        self.username = username
        self.email = email
    }
}
