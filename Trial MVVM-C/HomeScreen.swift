//
//  HomeScreen.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 21/01/22.
//

import Foundation
import UIKit

class HomeScreenView: UIView{
    
    @IBOutlet var HomeScreenContentView: UIView!
    @IBOutlet var CurrEmailLabel: UILabel!
    @IBOutlet var CurrUsernameLabel: UILabel!
    @IBOutlet var changeEmailButton: UIButton!
    @IBOutlet var changeUsernameButton: UIButton!
    
    var CurrEmail: String? {
        get {
            return CurrEmailLabel?.text
        }
        set {
            CurrEmailLabel.text = newValue
        }
    }
    
    var CurrUsername: String? {
        get {
            return CurrUsernameLabel?.text
        }
        set {
            CurrUsernameLabel.text = newValue
        }
    }
    
    required init?(coder homeDecoder: NSCoder) {
        super.init(coder: homeDecoder)
        initSubviews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    func initSubviews() {
        let nib = UINib(nibName: "HomeScreenView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        HomeScreenContentView.frame = bounds
        addSubview(HomeScreenContentView)
    }
    
}
