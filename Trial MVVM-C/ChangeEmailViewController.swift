//
//  ChangeEmailViewController.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 20/01/22.
//

import UIKit

class ChangeEmailViewController: UIViewController, CreateStoryVC {
    
    weak var coordinator: MainCoordinator?

    @IBOutlet var oldEmailTextField: UITextField!
    @IBOutlet var newEmailTextField: UITextField!
    @IBOutlet var confirmNewEmailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmEmailTapped(_ sender: Any) {
        let newEmail: String = "\(confirmNewEmailTextField.text!)"
        viewModel.email = "\(newEmail)"
        coordinator?.confirmEmail()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
