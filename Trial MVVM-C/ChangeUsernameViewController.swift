//
//  ChangeUsernameViewController.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 25/01/22.
//

import UIKit

class ChangeUsernameViewController: UIViewController, CreateStoryVC {

    weak var coordinator: MainCoordinator?
    
    @IBOutlet var oldUsernameTextField: UITextField!
    @IBOutlet var newUsernameTextField: UITextField!
    @IBOutlet var confirmNewUsernameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func confirmUsernameTapped(_ sender: Any) {
        let newUsername: String = "\(confirmNewUsernameTextField.text!)"
        viewModel.username = "\(newUsername)"
        coordinator?.confirmUsername()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
