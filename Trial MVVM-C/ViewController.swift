//
//  ViewController.swift
//  Trial MVVM-C
//
//  Created by BCA-GSIT-iMAC-11 on 20/01/22.
//

import UIKit

var viewModel = UserVM(username: "No Username Yet", email: "No Email Yet")

class ViewController: UIViewController, CreateStoryVC {

    
    //var viewModel = UserVM(username: "No Email Yet", email: "No Username Yet")
    weak var coordinator: MainCoordinator?
    
    @IBOutlet var currEmailLabel: UILabel!
    @IBOutlet var currUsernameLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        currEmailLabel.text = "\(viewModel.email)"
        currUsernameLabel.text = "\(viewModel.username)"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        currEmailLabel.text = "\(viewModel.email)"
        currUsernameLabel.text = "\(viewModel.username)"
    }

    @IBAction func ChangeEmailTapped(_ sender: Any) {
        coordinator?.changeEmail()
    }
    
    @IBAction func ChangeUsernameTapped(_ sender: Any) {
        coordinator?.changeUsername()
    }
    
    @IBAction func XIBHomeScreenTapped(_ sender: Any) {
        coordinator?.moveToXIBHomeScreen()
    }
}

